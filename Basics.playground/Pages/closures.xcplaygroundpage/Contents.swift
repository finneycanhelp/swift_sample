//: [Previous](@previous)
//: ## Closures
import Foundation

var someVar = 3

let someClosure = {
    someVar = someVar + 39
}

someClosure()

print(someVar)

//: [Next](@next)
