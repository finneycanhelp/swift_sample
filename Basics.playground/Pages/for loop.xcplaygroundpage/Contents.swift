//: [Previous](@previous)
//: ## for loop
import Foundation

for i in 10..<20 {
    // Psst! Ask to see the graph ;)
    let x = i * i * i * i * i
    print(x)
}

/*: Skipping `while` */
//: [Next](@next)
