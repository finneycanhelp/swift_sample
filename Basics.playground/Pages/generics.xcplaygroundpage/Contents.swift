//: [Previous](@previous)
//: ## Generics
import Foundation

// from htt p://j.mp/swiftGenerics

func swapTwoValues<T>(_ a: inout T, _ b: inout T) {
    let temporaryA = a
    a = b
    b = temporaryA
}

var first = 3; var second = 4
swapTwoValues(&first, &second)

print("first = \(first); second = \(second)")

//: [Next](@next)
