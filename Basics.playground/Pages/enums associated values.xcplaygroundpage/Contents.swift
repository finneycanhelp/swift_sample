//: [Previous](@previous)
//: ## Enumerations with Associated Values
// from http s://stackoverflow.com/questions/40665509/can-an-enum-contain-another-enum-values-in-swift
import Foundation

enum State {
    case started
    case succeeded
    case failed
}

enum ActionState {
    case state(value: State)
    case cancelled
}

class Action {

    var state: ActionState = .state(value: .started)

    func set(state: State) {
        self.state = .state(value: state)
    }

    func cancel() {
        self.state = .cancelled
    }

    var description:String {

        var result = "Action "
        switch state {

            case .state(value: .started):
                result += "\(state)"

            case .state(value: _):
                result += "\(state)"

            case .cancelled:
                result += "cancelled"
        }
        return result
    }
}

let obj = Action()
print(obj.description)
print()

obj.set(state: .failed)
print(obj.description)
print()

obj.set(state: .succeeded)
print(obj.description)
print()

obj.cancel()
print(obj.description)
print()
//: [Next](@next)
