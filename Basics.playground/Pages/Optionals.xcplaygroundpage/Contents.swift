//: [Previous](@previous)
//: ## Optionals
import Foundation

var optionalsCanBeNil: String? = nil

print(optionalsCanBeNil as Any)
// print(optionalsCanBeNil!) // I explode

/*: One way to ***unwrap an optional*** */
// optionalsCanBeNil = "something"

if let someString:String = optionalsCanBeNil {
    print(someString)
} else {
    print("it is nil")
}
//: [Next](@next)
