//: [Previous](@previous)
//: ## Constants, Variables, If
import Foundation

let someConstant:String = "I cannot change. 😂"
// someConstant = "will fail"

print(someConstant)

if someConstant.contains("😂") {
    print("Happy person")
}

//// *** var
//var 🤡 = "scary clown. change me"
////🤡 = "kids love me"
//
//if 🤡 == "kids love me" {
//    print("you are lying.")
//} else {
//    print(🤡)
//}
//: [Next](@next)

