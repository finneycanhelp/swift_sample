//: [Previous](@previous)
//: ## Functions
import Foundation

func someFunction() {   }

let canStore = someFunction
canStore()

func someFunctionWithArgs(something: String) {
    print(something + "\n")
}

func someFunctionReturningString() -> String {
    return "ok"
}

someFunction()

someFunctionWithArgs(something: "print me")

print(someFunctionReturningString())

//: [Next](@next)
