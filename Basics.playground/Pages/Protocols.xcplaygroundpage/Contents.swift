//: [Previous](@previous)
//: ## Protocols
import Foundation

protocol SomeProtocol {
    
    func someFunc()
}

class SomeClass: SomeProtocol {
    func someFunc() {
        print("stuff")
    }
}

let ok:SomeProtocol = SomeClass()
ok.someFunc()

//: [Next](@next)
