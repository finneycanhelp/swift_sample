//: [Previous](@previous)
//: ## Basic Types
import Foundation

let someInt: Int = 3
let anotherInt = 2

let someString: String = "ok"
let s = String("yes")

let someArray:[Int] = [1,2,3]
let anotherArray: Array<Int> = Array()

// dictionaries like Java Map
let someDictionary = ["someKey":"someValue"]
let someOptional:String? = someDictionary["someKey"]

print("The type is \(type(of:someOptional))")

print(someOptional as Any)
print("Just print the String '\(someOptional!)' now!")
//: [Next](@next)
