//: [Previous](@previous)
//: ## switch 
import Foundation

var someInt = 1

switch someInt {
case 3:
    print("three")
case 1:
    print("one")
case 10...19:
    print("Love these numbers")
default:
    print("whatever")
}

print() // add a blank line

var someString = "Etch who?"

switch someString {
case "knock":
    print("knock")
case "Who's there?":
    print("Etch")
case "Etch who?":
    print("Gesundheit")
default:
    print("funny")
}
//: [Next](@next)
