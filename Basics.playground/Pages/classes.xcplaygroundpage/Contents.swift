//: [Previous](@previous)
//: ## Classes
import Foundation

class SomeClass {
    
    func someFunction() {
        print("success")
    }
}

let someObject = SomeClass()

someObject.someFunction()
//: [Next](@next)