//: [Previous](@previous)
//: ## Inheritance
import Foundation
import UIKit

class Toy {
    var cost: Double = 0.0
}

class FidgetSpinner : Toy {
    func spin() { print("$\(cost) spinning") }
}

let someFidgetSpinner = FidgetSpinner()

someFidgetSpinner.cost = 6.99
someFidgetSpinner.spin()

//: [Next](@next)
