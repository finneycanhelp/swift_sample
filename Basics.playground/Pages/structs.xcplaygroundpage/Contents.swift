//: [Previous](@previous)
//: ## Structs
import Foundation

struct SomeStruct {
    
    var something: String
}

let someStruct = SomeStruct(something: "wow")

print(someStruct.something)

//: [Next](@next)
