//: [Previous](@previous)
//: ## Tuple Type
import Foundation

let coordinates:(Int, Int, Int, Int) = (4,2,9,4)
print("\(coordinates) \n")

var coupleStrings:(String, String) = ("first", "second")
print(coupleStrings)
print(coupleStrings.0)
print(coupleStrings.1)
print()

let (x, y) = coupleStrings
print("x: \(x); y: \(y) \n")

switch coupleStrings {
case ("first", "second"):
    print("Wow! 😊")
case ("first", _):
    print("got the first for sure")
default:
    print("default")
}
//: [Next](@next)

