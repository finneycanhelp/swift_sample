//: [Previous](@previous)
//: ## Protocol Extensions

import Foundation

protocol Spellcastable {

    func castSpell()
}

extension Spellcastable {
    
    func castSpell() { print("spell cast") }
}

protocol Eatable {
    
    func eat()
}

extension Eatable {
    func eat() { print("eating") }
}

class Wizard : Eatable, Spellcastable { }

class Barbarian : Eatable { }

Wizard().castSpell()
Wizard().eat()
Barbarian().eat()
//: [Next](@next)
