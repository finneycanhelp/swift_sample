/*:
 [Previous](@previous)
 ## Watching the Clock
 */
import Foundation

var formatter = DateFormatter()
formatter.timeStyle = .short

var timeString = "The time is: \(formatter.string(from:Date()))"
print(timeString)
/*:
 ![Hourglass](hourglass-presentation_200.jpg)
*/
//: [Next](@next)
