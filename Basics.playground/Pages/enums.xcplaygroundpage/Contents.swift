//: [Previous](@previous)
//: ## Enumerations
import Foundation

enum Fruit { case Apple, Orange, Grapes }

let someFruit : Fruit = .Apple
print(someFruit)

enum ApiGatewayError : Int {
    
    case unknownError = 0, notFound = 404, internalServerError = 500
}
/*: Convert a raw Int into an ***ApiGatewayError*** enum value */
let responseFromServer = 404 // Not Found

let apiGatewayError = ApiGatewayError(rawValue: responseFromServer) ?? .unknownError

print("\napiGatewayError = \(apiGatewayError)")
//: [Next](@next)
